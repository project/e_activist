<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function e_activist_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function e_activist_node_info() {
  $items = array(
    'activist_action' => array(
      'name' => t('E-Activist Action'),
      'module' => 'features',
      'description' => t('A webform that represents an e-activist action.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'activist_campaign_feed' => array(
      'name' => t('E-Activist Campaign Feed'),
      'module' => 'features',
      'description' => t('Used to store information about each e-activist campaign feed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
