<?php

/**
 * @file
 * Admin functions for the e-activist module.
 *
 */
 
/**
 * Settings form for e-activist actions.
 */
function e_activist_settings_form() {
/*
    $form['e_activist_demo_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Submit actions in demo mode'),
    '#default_value' => variable_get('e_activist_demo_mode', TRUE),
    '#description' => t('When checked, submitted actions will be sent in demo mode. Demo mode allows you to submit test actions before going live.'),
  );
*/  
  $form['e_activist_api_url'] = array(
    '#type' => 'textfield',
    '#title' => 'API URL',
    '#default_value' => variable_get('e_activist_api_url', 'http://e-activist.com/ea-action/action'),
    '#description' => t('The Advocacy Online API endpoint.'),
  );
  
  return system_settings_form($form);
}
